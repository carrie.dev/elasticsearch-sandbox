# STEP 1: Bind internal K8S 9000 port to external and forward port for accessing Cerebro:
invoke-expression 'cmd /c start powershell -NoExit -Command {                           
    $host.UI.RawUI.WindowTitle = "K8S: Port-Forwarding Cerebro...";                                
    $host.UI.RawUI.BackgroundColor = "DarkGreen";
    kubectl -n logging port-forward deploy/cerebro 9000:9000;                                                                              
}';

# STEP 2: Launch browser, opening Cerebro URL:
Start-Process 'http://localhost:9000'