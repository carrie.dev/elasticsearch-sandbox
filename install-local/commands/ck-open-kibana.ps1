# STEP 1: Bind internal K8S 5601 port to external and forward port for accessing Kibana:
# Simply Way:
#Start-Process powershell { kubectl -n logging port-forward deploy/kibana 5601:5601 }

# Fancy Way w/a Title:
invoke-expression 'cmd /c start powershell -NoExit -Command {                           
    $host.UI.RawUI.WindowTitle = "K8S: Port-Forwarding Kibana...";                                
    $host.UI.RawUI.BackgroundColor = "DarkBlue";
    kubectl -n logging port-forward deploy/kibana 5601:5601;                                                                              
}';

# STEP 2: Launch browser, opening Kibana URL:
Start-Process 'http://localhost:5601/app/kibana'