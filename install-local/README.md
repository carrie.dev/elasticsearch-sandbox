# Elasticsearch Kubernetes Setup

## Part 1 - Install Docker-Desktop

Install [Docker-Desktop](https://www.docker.com/products/docker-desktop) for your OS.

- Enable Hyper-V and Containers features when prompted!

Once Docker-Desktop is running, click on the tools try icon and open settings to enable Kubernetes:
![How to enable Kubernetes in Docker for Desktop](../images/docker-desktop-k8s-setup.png?raw=true)

## Part 2 - Run Setup Files

There are seven setup files to run in this order:

- logging-namespace.yaml
- elasticsearch-service.yaml
- elasticsearch-statefulset.yaml
- kibana-deployment.yaml
- kibana-service.yaml
- cerebro-service.yaml
- cerebro-deployment.yaml

### Steps to Execute

1.  Open a terminal window.
2.  Change directory to `install-local`.
3.  Run this command:
    - `kubectl apply -f logging-namespace.yaml`
    - `kubectl apply -f .` to run all the commands in the folder, things will just not come up until the needed pieces are running.
4.  If you want to start over, simply delete all the configuration completed via the yaml files with this command:
    - `kubectl delete -f .`
    - If not using the default: emptyDir: {} for storage, make sure to delete all the persistent volume claims related so the next time around you avoid unbound vc errors:
        - `kubectl get pvc -n logging`
        - `kubectl delete elasticsearch-elasticsearch-0 pvc -n logging`
            - Where the above is the volumeclaimname-pod name & repeat for other two pods.
            - NOTE: pvc are tied to `kubectl get sc` to show the available storage container.
5.  To see what has been auto-installed for you, run this:
    - `kubectl get all -n logging`
        - To see everything in the cluster: - `kubectl get all --all-namespaces`
![Kubernetes Setup of Elasticsearch, Kibana, & Cerebro](../images/kubernetes-resources.png?raw=true)

Did anything fail to start running? Try the following two commands to help research:
    1. kubectl describe pod/podNameHere -n logging
        - `kubectl describe pod/elasticsearch-0 -n logging`
    2. kubectl logs -n logging pod/podNameHere
        - `kubectl logs -n logging pod/elasticsearch-0`

## Part 3 - Access Via Browser

Open a new terminal window for each proxy to access from Kubernetes:
- Proxy Elasticsearch via: `kubectl -n logging port-forward elasticsearch-0 9200:9200`
    - Then open browser: http://localhost:9200 or hit the API directly http://127.0.0.1:9200/_cluster/state?pretty
- Proxy Kibana via: `kubectl -n logging port-forward deployment/kibana 5601:5601` 
    - Then open browser: http://localhost:5601/app/kibana
- Proxy Cerebro via: `kubectl -n logging port-forward deployment/cerebro 9000:9000` 
    - Then open browser: http://localhost:9000

Alternatively, the commands folder provides helper scripts to launch each site via powershell.

Steps to setup Windows to use scripts:
1) Create `commands` folder on root location for reuse purposes.
2) Copy contents of repo's commands folder into local folder just created.
3) Add folder to environment `PATHS` to have access gobally.
4) Open terminal and type `ck-open` + tab to toggle through options.

Each scripte is setup to open a new terminal window to port-forward from Kubernetes to your machine.  Then a second command will launch a new browser window with the URL for the desired site.

## References

Files derived from the following sources:

- Elasticsearch & Kibana: https://github.com/kubernetes/kubernetes/tree/master/cluster/addons/fluentd-elasticsearch
- Cerebro: https://github.com/kayrus/elk-kubernetes/blob/master/es5/es-cerebro.yaml