# Elasticsearch Sandbox

Exploring the many facets Elasticsearch.

## Setup
First, you need to install Elasticsearch locally to play with the features.  I will highlight three options below for you to choose from:

1) Download .zip files from Elastic.io and manually run a command window calling batch files.  The downside is that data is lost once the cmd window closes. 
    - Download Elasticsearch: https://www.elastic.co/downloads/elasticsearch
        - Run `bin/elasticsearch` (or `bin\elasticsearch.bat` on Windows)
    - Download Kibana: https://www.elastic.co/downloads/kibana
        - Run `bin/kibana` (or `bin\kibana.bat` on Windows)
2) Run Docker containers.
    - Content below was summarized from these [Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html) & [Kibana](https://www.elastic.co/guide/en/kibana/current/docker.html) docs.
    - Results of scripts below will:
        - Add 2 images: `docker images`
        - Add 2 containers: `docker ps`

    - `docker pull docker.elastic.co/elasticsearch/elasticsearch:7.5.1`
    - `docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.5.1`
    - `docker pull docker.elastic.co/kibana/kibana:7.5.1`
    - docker run --link YOUR_ELASTICSEARCH_CONTAINER_NAME_OR_ID:elasticsearch -p 5601:5601 {docker-repo}:{version}
        - IE: `docker run --link 36f1ef122edb:elasticsearch -p 5601:5601 docker.elastic.co/kibana/kibana:7.5.1`
        - NOTE: Run `docker ps` to get containerId to up ^
    - To kill the running containers, do kibana first and then es via: `docker stop YOUR_ELASTICSEARCH_CONTAINER_ID`
3) Run setup in Kubernetes.
    - See [README](https://gitlab.com/carrie.dev/elasticsearch-sandbox/blob/master/install-local/README.md) in install-local folder for further instructions and how to access.

Both options 1 & 2 above allow you to quickly see the fruits of your labor by opening up Elasticsearch at `localhost:9200` & Kibana at `localhost:5601`.

## Overview

I will assume for the remaining parts that you choose my favorite setup - option 3 going forward. :)

Installed components:

```mermaid
graph TD;
    A[Kibana]-->|UI Tool| B[Elasticsearch]
    C[Cerebro]-->|Admin Tool| B[Elasticsearch]
```
