# STEP 1: Bind internal K8S 9200 port to external and forward port for accessing Elasticsearch:
invoke-expression 'cmd /c start powershell -NoExit -Command {                           
    $host.UI.RawUI.WindowTitle = "K8S: Port-Forwarding Elasticsearch...";                                
    $host.UI.RawUI.BackgroundColor = "DarkRed";
    kubectl -n logging port-forward pod/elasticsearch-0 9200:9200;                                                                              
}';

# STEP 2: Launch browser, opening Elasticsearch URL:
Start-Process 'http://localhost:9200'